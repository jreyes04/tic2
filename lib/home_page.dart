import 'package:flutter/material.dart';
import 'package:tic_app/stats_page.dart';
import 'package:tic_app/feed_page.dart';
import 'profile_page.dart';
import 'services/auth.dart';
import 'location_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'services/user.dart';
import 'stats_page_client.dart';

class HomePage extends StatefulWidget {
  HomePage({this.auth, this.onSignedOutCallback});
  final BaseAuth auth;
  final VoidCallback onSignedOutCallback;
  @override
  State<StatefulWidget> createState() => _HomePageState();
}

enum TypeUser { owner, client }

class _HomePageState extends State<HomePage> {
  int currentTab = 1;
  final _db = Firestore.instance;
  User _user;
  FirebaseUser firebaseUser;
  QuerySnapshot snapshot;
  String userId, userName, userLastName, userType, identification, userEmail;
  TypeUser typeUser;
  List<Widget> pages;
  var docRef;
  Future<void> fetchUserData(String uid) async {
    // QuerySnapshot dataQuery;
  try {
        Firestore.instance
        .collection('users')
        .where("id", isEqualTo: uid)
        .snapshots()
        .listen((data) {
      setState(() {
        print("$data////////////////////////////////");
        this.snapshot = data;
        print("$snapshot");
        print("${this.snapshot}");
        data.documents.forEach((doc) {
          _user = User(doc["nombre"], doc["apellido"], doc["id"],
              doc["cedula_ID"], doc["cliente"]);
          print(_user.cliente);

          if (_user.cliente == true)
          typeUser = TypeUser.client;
          else
            typeUser = TypeUser.owner;
        });
      });
      
    });

  } catch (e) {
  }
  }

  ////////////////////////////////////////
  fetchUser() async {
    try {
      //  await new Future.delayed(const Duration(milliseconds: 2000));
      widget.auth.currentUser().then((user) async {

        fetchUserData(user.uid);
        setState(() {
          userId = user.uid;

        });
      });
    } catch (e) {
      print("$e");
    }
  }
  void switchUser() async{

    setState(() {
      typeUser = typeUser == TypeUser.client?TypeUser.owner: TypeUser.client;
    });
    
  }

  @override
  initState() {
    // print(this.snapshot.documentID);
    fetchUser();
    switchUser();
    super.initState();
  }

  Widget buildBoxes() {
    //method to navegate across the views
    switch (currentTab) {
      case 0:
        return typeUser == TypeUser.owner
            ? StatsPage() //auth: widget.auth, idUsuario: _user.id,
            :  StatSpageUser(id: userId, auth: Auth(),);
      case 1:
        return LocationPage();
      case 2:
        return FeedPage();
      default:
        return Text("default");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Rent A Car'),
        actions: <Widget>[],
      ),
      drawer: Drawer(
        child: ListView(children: <Widget>[
          UserAccountsDrawerHeader(
            onDetailsPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProfilePage()),
              );
            },
            currentAccountPicture: GestureDetector(
              child: CircleAvatar(
                backgroundImage: AssetImage('images/avatar.png'),
              ),
            ),
            accountName: Text(""), //userName
            accountEmail: Text(""), //userEmail
            decoration: BoxDecoration(
              color: Colors.teal,
            ),
          ),
          /*StreamBuilder(
            stream: _db.collection('users').where("id", isEqualTo: userId).snapshots(),
            builder: (context, snapshot){
              if(snapshot.hasData){
                return snapshot.data.documents.map((doc) => buildDrawer(doc));
              }
              else return CircularProgressIndicator();
            },
          ),*/
          // ListTile(
          //   title: Text('Settings'),
          //   trailing: Icon(Icons.settings),
          // ),
          ListTile(
            title: Text('Switch user'),
            trailing: Icon(Icons.account_circle),
            onTap: switchUser,
          ),
          ListTile(
            title: Text('Logout'),
            trailing: Icon(Icons.power),
            onTap: widget.onSignedOutCallback,
          ),
        ]),
      ),
      body: buildBoxes(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentTab,
        onTap: (int index) {
          setState(() {
            currentTab = index;
          });
        },
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: barIcon(), title: Text('Add')),
          BottomNavigationBarItem(
              icon: Icon(Icons.location_on), title: Text('Location')),
          BottomNavigationBarItem(
              icon: Icon(Icons.directions_car), title: Text('Cars')),
        ],
      ),
    );
  }

  Icon barIcon(){
      if(typeUser == TypeUser.owner){
        return Icon(Icons.plus_one);
      }else{
        return Icon(Icons.payment);
      }
  }
  Text textUser(){
          if(typeUser == TypeUser.owner){
        return Text('Add');
      }else{
        return Text('Stats');
      }
  }

 void updateData(DocumentSnapshot doc) async {
    await _db
        .collection('cars')
        .document(doc.documentID)
        .updateData({'rentado': true});
  }
}

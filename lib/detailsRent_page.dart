import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';
import 'services/auth.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'services/crud.dart';
import 'package:sweetalert/sweetalert.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:date_format/date_format.dart';

class DetailsRent extends StatefulWidget {
  DetailsRent({this.crudMethods, this.auth, this.id, this.doc});
  final BaseAuth auth;
  final String id;
  final CrudMethods crudMethods;
  final String doc;
  @override
  _DetailsRentState createState() => _DetailsRentState();
}

class _DetailsRentState extends State<DetailsRent> {
  var userId, difference, id, costo;
  final _db = Firestore.instance;
  final formKey = GlobalKey<FormState>();
  final formats = {
    InputType.both: DateFormat("EEEE, MMMM d, yyyy 'at' h:mma"),
    InputType.date: DateFormat('yyyy-MM-dd'),
    InputType.time: DateFormat("HH:mm"),
  };

  CrudMethods _crudMethods = CrudMethods();

  void validateAndSubmit() {
    //print("AQUI ESTOOOOOOOOOOOYYYYYYY");
    //print("DATOOOOOOOOOOO"+date.toString());
    widget.auth.currentUser().then((user) {
      setState(() {
        try {
          Map<String, dynamic> rentData = <String, dynamic>{
            "idClient": user.uid,
            "dateOfRent": this._date,
            "dateInitial": this.date,
            "dateFinal": this.dateEnd,
            //"idClient": ,
            //"total": ,
          };
          _crudMethods.addRentData(rentData).then((result) {
            print('success');
            SweetAlert.show(context,
                title: 'Succesfully added',
                style: SweetAlertStyle.success,
                subtitle: '😁');
          });
        } catch (e) {
          SweetAlert.show(context,
              title: 'Try again later',
              style: SweetAlertStyle.error,
              subtitle: 'Something went wrong, please try again');
          print('<------------Error------------->: $e ');
        }
        updateData(widget.doc);
  print("*****************${widget.doc}********************************");


      });
    });
  }

  Future<DocumentSnapshot> getData(DocumentSnapshot doc) {
    _db
        .collection('cars')
        .document(doc.documentID)
        .get();
    print("EEEEEEE"+costo);

    return costo;
  }

  void totalCost() {
    print(date);
    print(dateEnd);
    difference = dateEnd.difference(date).inHours;
    print("AAAAA " + difference.toString());
    
    FirebaseAuth.instance.currentUser().then((onValue){
      print(Firestore.instance.collection('cars').document().documentID);
    });

  }

  @override
  void initState() {
    super.initState();
  }

  /////////////////////////////////////////////////////////////////////////////////////////////

  InputType inputType = InputType.both;
  bool editable = true;
  DateTime date;
  DateTime dateEnd;
  TimeOfDay _time = TimeOfDay.now();
  DateTime _date = DateTime.now();

  @override
  Widget build(BuildContext context) {
    //validateAndSave();
    return Scaffold(
      appBar: AppBar(
        title: Text("Rent Details"),
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          DateTimePickerFormField(
            inputType: inputType,
            format: formats[inputType],
            editable: editable,
            decoration: InputDecoration(
                labelText: 'Initial Date and Time of the Rent',
                hasFloatingPlaceholder: false),
            onChanged: (dt) => setState(() => date = dt),
          ),
          DateTimePickerFormField(
            inputType: inputType,
            format: formats[inputType],
            editable: editable,
            decoration: InputDecoration(
                labelText: 'Final Date and Time of the Rent',
                hasFloatingPlaceholder: false),
            onChanged: (dtF) => setState(() => dateEnd = dtF),
          ),
          Text("Your total cost is: " + difference.toString()),
          RaisedButton(
            child: Text("Check your price"),
            onPressed: totalCost,
          ),
          RaisedButton(
            child: Text("Rent"),
            onPressed: validateAndSubmit,
          )
        ],
      ),
    );
  }
    void updateData(String doc) async {
    await _db
        .collection('cars')
        .document(doc)
        .updateData({'rentado': true});
  }
}

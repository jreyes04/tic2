import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sweetalert/sweetalert.dart';
import 'detailsRent_page.dart';
import 'services/auth.dart';

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  String id, car_model;
  final _db = Firestore.instance;

  Card buildItem(DocumentSnapshot doc) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Text("${doc.data['marca'].toUpperCase()} ${doc.data['modelo']}"),
            ),
            SizedBox(
              height: 10,
            ),
            Text("${doc.data['placa']}"),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.attach_money),
                        Text("${doc.data['costo']}"),
                      ],
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.people),
                        SizedBox(
                          width: 5,
                        ),
                        Text("${doc.data['capacidad']}"),
                      ],
                    )
                  ],
                ),
                FlatButton(
                  child: Text(
                    "Rent",
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  onPressed: () => showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: new Text("Terms and Conditions"),
                            content: new Text("You must have a driver license... Blah blah blah"),
                            actions: <Widget>[
                              new FlatButton(
                                child: new Text("Close"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              FlatButton(
                                child: Text("Agree"),
                                onPressed: () => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            DetailsRent(
                                                auth:
                                                    Auth(), doc: doc.documentID,))), //updateData(doc),,
                              ),
                            ],
                          );
                        },
                      ),
                  color: Colors.teal,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.all(5),
        children: <Widget>[
          StreamBuilder<QuerySnapshot>(
            stream: _db
                .collection('cars')
                .where('rentado', isEqualTo: false)
                .snapshots(),
            builder: (context, snapshot) {
              if (snapshot.data == null) {
                return CircularProgressIndicator();
                /*child: Center(
                    child: Text("Apparently there are no available cars in the moment, try again later.. 😞", 
                    style: TextStyle(color: Colors.black),
                    ),            
                  ),*/
              } else {
                return Column(
                  children: snapshot.data.documents
                      .map((doc) => buildItem(doc))
                      .toList(),
                );
              }
            },
          ),
        ],
      ),
    );
  }

  void updateData(DocumentSnapshot doc) async {
    await _db
        .collection('cars')
        .document(doc.documentID)
        .updateData({'rentado': true});
  }
}
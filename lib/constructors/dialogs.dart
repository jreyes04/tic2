import 'package:flutter/material.dart';

class Dialogs {
  Future<void> errorDialog(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                child: Text('Close'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }
}

// class Dialogs {
//   information(BuildContext context, String title, String description) {
//     return showDialog(
//         context: context,
//         barrierDismissible: true,
//         builder: (BuildContext context) {
//           return AlertDialog(
//             title: Text(title),
//             content: SingleChildScrollView(
//               child: ListView(
//                 children: <Widget>[
//                   Text(description),
//                 ],
//               ),
//             ),
//             actions: <Widget>[
//               FlatButton(
//                 child: Text("Close"),
//                 onPressed: () => Navigator.of(context).pop(),
//               )
//             ],
//           );
//         });
//   }
// }

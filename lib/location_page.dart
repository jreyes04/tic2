import 'package:flutter/material.dart';
import 'currentMethods.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:sweetalert/sweetalert.dart';


class LocationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LocationPage();
  }
}

class _LocationPage extends State<LocationPage> {
  @override
  void initState() {
    super.initState();
  }

  GoogleMapController _googleMapController;
  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(6.280615, -75.611155),
    zoom: 11.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(6.280615, -75.611155),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: GoogleMap(
        mapType: MapType.hybrid,
        initialCameraPosition: _kGooglePlex,
//        onMapCreated: (GoogleMapController controller) {
//         _googleMapController.moveCamera(
//
//         );
//        },
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => SweetAlert.show(context,
            title: 'Excelente',
            style: SweetAlertStyle.success,
            subtitle: 'The email or password may not be correct'),
        label: Text('Open'),
        icon: Icon(Icons.directions_boat),
      ),
    );
  }

//  Future<void> _goToTheLake() async {
//    final GoogleMapController controller = await _googleMapController.future;
//    controller.animateCamera(CameraUpdate.newCameraPosition(_kLake));
//  }
}


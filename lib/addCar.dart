import 'package:flutter/material.dart';
import 'services/crud.dart';
import 'package:sweetalert/sweetalert.dart';
import 'services/auth.dart';
import 'services/car.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:location/location.dart';
// import 'package:geolocator/geolocator.dart';

class AddCarPage extends StatefulWidget {
  AddCarPage({this.crudMethods, this.auth, this.idUsuario});
  final BaseAuth auth;
  final String idUsuario;

  final CrudMethods crudMethods;
  @override
  State<StatefulWidget> createState() => _AddCarPageState();
}

class _AddCarPageState extends State<AddCarPage> {
  final formKey = GlobalKey<FormState>();
  var _idOwner,
      _car,
      _plate,
      _cost,
      _brand,
      _capacity,
      capacityDropDown,
      currentLocation;
  List<DropdownMenuItem<int>> dropDown;
  Car car;
  void loadData() {
    dropDown = [];
    dropDown.add(DropdownMenuItem(
      child: Text("2"),
      value: 2,
    ));
    dropDown.add(DropdownMenuItem(
      child: Text("4"),
      value: 4,
    ));
    dropDown.add(DropdownMenuItem(
      child: Text("5"),
      value: 5,
    ));
  }

  CrudMethods _crudMethods = CrudMethods();

  bool validateAndSave() {
    //funcion de validacion de estado para el logueo
    final form = formKey.currentState;
    if (form.validate()) {
      form.save(); //se guardan los datos una vez se verifican
      return true;
    } else {
      return false;
    }
  }

  void validateAndSubmit() async {
    //funcion asincrona de validacion
    if (validateAndSave()) {
      try {
        Map<String, dynamic> carData = <String, dynamic>{
          // "idOwner": this.widget.userId,
          "marca": this._brand,
          "modelo": this._car,
          "rentado": false,
          "placa": this._plate,
          "costo": this._cost,
          "capacidad": this._capacity,
          "idOwner": this._idOwner
        };
        _crudMethods.addCarData(carData).then((result) {
          print('success');
          SweetAlert.show(context,
              title: 'Succesfully added',
              style: SweetAlertStyle.success,
              subtitle: '😁');
        });
        formKey.currentState.reset();
      } catch (e) {
        SweetAlert.show(context,
            title: 'Try again later',
            style: SweetAlertStyle.error,
            subtitle: 'Something went wrong, please try again');
        print('<------------Error------------->: $e ');
      }
    }
  }

  // Future<void> fetchCarData(String uid) async {
  //   // QuerySnapshot dataQuery;
  //   Firestore.instance
  //       .collection('cars')
  //       .where("id", isEqualTo: _idOwner)
  //       .snapshots()
  //       .listen((data) {
  //     setState(() {
  //       print("$data////////////////////////////////");
  //       data.documents.forEach((doc) {
  //         car = Car(doc['marca'], doc['modelo'], doc['ownerId'], doc['placa'],
  //             doc['capacidad'], doc['costo'], doc['estado']);
  //             print("$car***********************");
  //       });
  //     });
  //   });
  //   // return dataQuery;
  // }

///////////////////////////
  @override
  void initState() {
    setState(() {
      capacityDropDown = 'Capacity';
    });
    // fetchCarData(_idOwner);
    // print("${car.capacidad}------------------------");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    loadData();
    widget.auth.currentUser().then((user) {
      setState(() {
        _idOwner = user.uid;
      });
    });
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Add your car "),
      ),
      body: Container(
        child: Form(
            key: formKey,
            child: Column(
              //column
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: buildInputs() + buildSubmitsButtons(),
            )),
      ),
    );
  }

  List<Widget> buildInputs() {
    return [
      TextFormField(
          decoration: InputDecoration(
            labelText: 'Vehicle brand',
          ),
          validator: (value) =>
              value.isEmpty ? 'Brand can\'t be empty' : null, //validator
          onSaved: (value) {
            _brand = value;
          }),
      SizedBox(height: 10),
      TextFormField(
          decoration: InputDecoration(
            labelText: 'Model',
          ),
          validator: (value) =>
              value.isEmpty ? 'Model can\'t be empty' : null, //validator
          onSaved: (value) {
            _car = value;
          }),
      SizedBox(height: 10),
      TextFormField(
          decoration: InputDecoration(
            labelText: 'License plate',
          ),
          validator: (value) => value.isEmpty
              ? 'License plate can\'t be empty'
              : null, //validator
          onSaved: (value) {
            _plate = value;
          }),
      SizedBox(height: 10),
      TextFormField(
          decoration: InputDecoration(
            labelText: 'Cost by hour (in COP)',
          ),
          validator: (value) =>
              value.isEmpty ? 'Cost can\'t be empty' : null, //validator
          onSaved: (value) {
            _cost = value;
          }),
      SizedBox(height: 10),
      DropdownButton(
        items: dropDown,
        hint: Text(capacityDropDown),
        onChanged: (value) {
          _capacity = value;
          setState(() {
            switch (value) {
              case 2:
                capacityDropDown = "2";
                break;
              case 4:
                capacityDropDown = "4";
                break;
              case 5:
                capacityDropDown = "5";
                break;
              default:
                capacityDropDown = "Capacity";
            }
            // capacityDropDown = "Capacity";
          });
        },
      ),
      SizedBox(height: 10),
    ];
  }

  List<Widget> buildSubmitsButtons() {
    return [
      SizedBox(
        height: 10,
      ),
      RaisedButton(
        //button create account
        child: Text(
          'Add car',
          style: TextStyle(fontSize: 20.0),
        ),
        onPressed:
            validateAndSubmit, //se asume es un metodo por ende no se usa ()
      ),
    ];
  }
}

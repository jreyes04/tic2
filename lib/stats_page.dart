import 'package:flutter/material.dart';
import 'services/crud.dart';
import 'package:sweetalert/sweetalert.dart';
import 'services/auth.dart';
import 'services/car.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'addCar.dart';

class StatsPage extends StatefulWidget {
  StatsPage({this.crudMethods, this.auth, this.idUsuario});
  final BaseAuth auth;
  final String idUsuario;

  final CrudMethods crudMethods;
  @override
  State<StatefulWidget> createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  final formKey = GlobalKey<FormState>();
  var _idOwner,
      _car,
      _plate,
      _cost,
      _brand,
      _capacity,
      capacityDropDown,
      currentLocation;
  List<DropdownMenuItem<int>> dropDown;
  Car car;
  void loadData() {
    dropDown = [];
    dropDown.add(DropdownMenuItem(
      child: Text("2"),
      value: 2,
    ));
    dropDown.add(DropdownMenuItem(
      child: Text("4"),
      value: 4,
    ));
    dropDown.add(DropdownMenuItem(
      child: Text("5"),
      value: 5,
    ));
  }

  CrudMethods _crudMethods = CrudMethods();

   String getUserData() {
    String userid;
    FirebaseAuth.instance.currentUser().then((onValue){
      //print(onValue.email);
      _idOwner = onValue.uid;
      print("AIUAISUIDUASIUDAOS"+_idOwner);
    });
    return userid;
  }

///////////////////////////
  @override
  void initState() {
    setState(() {
      _idOwner = widget.idUsuario;
    });

    super.initState();
  }



  Card buildItem(DocumentSnapshot doc) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              //child: Text("${doc.data['marca'].toUpperCase()} ${doc.data['modelo']}"),
            ),
            SizedBox(
              height: 10,
            ),
            Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.attach_money),
                        Text("${doc.data['costo']}"),
                      ],
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.people),
                        SizedBox(
                          width: 5,
                        ),
                        Text("${doc.data['capacidad']}"),
                      ],
                    )
                  ],
                ),
                Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(Icons.info),
                        SizedBox(
                          width: 5,
                        ),
                       Text("${doc.data['placa']}"),
                      ],
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    FlatButton(
                      onPressed: () => null, //updateData(doc),
                      child: Text(
                        "Rent",
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      color: Colors.teal,
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    loadData();
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("My Car"),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.all(5),
        children: <Widget>[
          StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance
                .collection('cars')
                .where('idOwner', isEqualTo: _idOwner)
                .snapshots(),
            builder: (context, snapshot) {
              if (snapshot.data == null) {
                return CircularProgressIndicator();

              } else {
                return Column(
                  children: snapshot.data.documents
                      .map((doc) => buildItem(doc))
                      .toList(),
                );
              }
            },
          ),
          Container(
        child: Form(
          key: formKey,
          child: Column(
            //column
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: buildSubmitsButtons(),
    ),
        ),
      ),
        ],
      ),
    
    );
  }

 
  List<Widget> buildSubmitsButtons() {
    return [
      SizedBox(
        height: 10,
      ),
      RaisedButton(
        //button create account
        child: Text(
          'Add car',
          style: TextStyle(fontSize: 20.0),
        ),
        onPressed:
            //getUserData,
            () => Navigator.push(context, MaterialPageRoute(builder:  (context) => AddCarPage( auth: Auth(), idUsuario: _idOwner))), //se asume es un metodo por ende no se usa ()
      ),
    ];
  }
}

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'services/auth.dart';

class StatSpageUser extends StatefulWidget {
  StatSpageUser({this.id, this.auth});
  final String id;
  final BaseAuth auth;
  @override
  _StatSpageUserState createState() => _StatSpageUserState();
}

class _StatSpageUserState extends State<StatSpageUser> {
  
  Card buildItem(DocumentSnapshot doc) {
    return Card(

        child: Column(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.directions_car),
              title: Text("${doc.data['dateInitial'].toString()}"),
            )
          ],
        ),
      // child: Padding(
      //   padding: const EdgeInsets.all(8.0),
      //   child: Column(
      //     crossAxisAlignment: CrossAxisAlignment.start,
      //     children: <Widget>[
      //       Center(
      //         //child: Text("${doc.data['marca'].toUpperCase()} ${doc.data['modelo']}"),
      //       ),
      //       SizedBox(
      //         height: 10,
      //       ),
      //       Divider(),
      //       Row(
      //         mainAxisAlignment: MainAxisAlignment.spaceAround,
      //         children: <Widget>[
      //           Column(
      //             children: <Widget>[
      //               Row(
      //                 children: <Widget>[
      //                   Icon(Icons.attach_money),
      //                   Text("${doc.data['costo']}"),
      //                 ],
      //               )
      //             ],
      //           ),
      //           Column(
      //             children: <Widget>[
      //               Row(
      //                 children: <Widget>[
      //                   Icon(Icons.people),
      //                   SizedBox(
      //                     width: 5,
      //                   ),
      //                   Text("${doc.data['capacidad']}"),
      //                 ],
      //               )
      //             ],
      //           ),
      //           Column(
      //             children: <Widget>[
      //               Row(
      //                 children: <Widget>[
      //                   Icon(Icons.info),
      //                   SizedBox(
      //                     width: 5,
      //                   ),
      //                  Text("${doc.data['placa']}"),
      //                 ],
      //               )
      //             ],
      //           ),
      //           Row(
      //             children: <Widget>[
      //               FlatButton(
      //                 onPressed: () => null, //updateData(doc),
      //                 child: Text(
      //                   "Rent",
      //                   style: TextStyle(
      //                     color: Colors.white,
      //                   ),
      //                 ),
      //                 color: Colors.teal,
      //               ),
      //             ],
      //           )
      //         ],
      //       ),
      //     ],
      //   ),
      // ),
    );
  }

  @override
  void initState() {
    print(("${widget.id}"));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text("Registers "),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.all(5),
        children: <Widget>[
          StreamBuilder<QuerySnapshot>(
            stream: Firestore.instance
                .collection('rents')
                .where('idClient', isEqualTo: widget.id)
                .snapshots(),
            builder: (context, snapshot) {
              if (snapshot.data == null) {
                return CircularProgressIndicator();
              } else {
                return Column(
                  children: snapshot.data.documents
                      .map((doc) => buildItem(doc))
                      .toList(),
                );
              }
            },
          ),
        ],
      ),
     
      );
  }
}

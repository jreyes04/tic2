import 'package:flutter/material.dart';
import 'services/auth.dart';
import 'services/crud.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({this.crudMethods});
  final CrudMethods crudMethods;
  @override
  State createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>{
  //CrudMethods _crudMethods = CrudMethods();
  var _userName, _userLastName, _userEmail, _userCC, _correoTest;
  
  String getUserData() {
    String _correo,_name;
    FirebaseAuth.instance.currentUser().then((onValue){
      //print(onValue.email);
      _correo = onValue.email;
      //print(_correo);
      
      Firestore.instance.collection('users').where("id", isEqualTo: onValue.uid).snapshots().listen((data) => 
        data.documents.forEach((doc) => 
        _name = doc["nombre"]));
      //print("HOLAA"+_name);
      this._correoTest = onValue.email;
      
      setState(() {
              _userEmail = onValue.email;
              _userName = _name;
            });
      //Firestore.instance.collection('users').where("id", isEqualTo: onValue.uid).snapshots().listen((data) => data.documents.forEach((doc) => print(doc["apellido"])));
      //Firestore.instance.collection('users').where("id", isEqualTo: onValue.uid).snapshots().listen((data) => data.documents.forEach((doc) => print(doc["cc"])));
    });
    return _correo;
  }

  @override
  void initState(){
    super.initState();
    setState(() {
          this._userEmail = this.getUserData().toString();
          this._userName = this.getUserData().toString();
          //widget.crudMethods.getUserData().toString(); 
          //_userName = _crudMethods.getUserData().toString();
        //print("Correo $_userEmail");
        //print("Nombre "+_userName);
        });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User Profile"),
        centerTitle: true,
      ),
      body: Center(
        child: Stack(
          children: [
          _buildStack(),
          ],
        ),
      ),
    );
  }
  Widget _buildStack() => ListView(
    children: [
      Center(
          child: CircleAvatar(  
          backgroundImage: AssetImage('images/avatar.png'),
          radius: 70,    
        ),
      ),
      Divider(),
      Text("Holi "+ this._userEmail, textAlign: TextAlign.center),
      //Text("Nombre "+ this._userName, textAlign: TextAlign.center),
    ],
    padding: EdgeInsets.all(20),
  );
}
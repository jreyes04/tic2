import 'package:flutter/material.dart';

titleContainer(String title, Color colorx) {
  //Retorna un container con el texto que se introduzca (tipo Banner con el color de fondo deseado)
  return Container(
      padding: EdgeInsets.all(7.0),
      color: colorx,
      child: Text(
        title,
        style: TextStyle(
          color: Colors.white,
          fontSize: 25.0,
        ),
        textAlign: TextAlign.center,
      ));
}

imageContainer(String src) {
  //Retorna un Container con una imagen
  return Container(
    decoration: BoxDecoration(
      image: DecorationImage(
        fit: BoxFit.cover,
        image: NetworkImage(src),
      ),
    ),
    height: 240.0,
  );
}

titleAndSubContainer(String title, String subtitle) {
  //Retorna una fila con título y subtítulo 
  return Row(
    children: [
      Expanded(
          child: Column(
        children: [
          Container(
            child: Text(
              title,
              style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),
            ),
          ),
          Text(
            subtitle,
            style: TextStyle(fontSize: 10.0),
          ),
        ],
      )),
    ],
  );
}

currentExpanded(String title, String subtitle, String miniString,
    IconData iconx, MaterialColor colorx) {
  //Retorna una fila especial con 3 textos y 1 ícono con el color que se desee
  return Container(
      padding: EdgeInsets.all(7.0),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    title,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  subtitle,
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          Text(
            miniString,
            style: TextStyle(
              color: Colors.grey[500],
            ),
          ),
          Icon(
            iconx,
            color: colorx,
          )
        ],
      ));
}

sectionDivider(String title, String subtitle) {
//Igual que titleAndSubContainer pero más pequeño y con divisores
  return Row(
    children: [
      Expanded(
          child: Column(
        children: [
          Divider(),
          Container(
            child: Text(
              title,
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ),
          Text(
            subtitle,
            style: TextStyle(fontSize: 10.0),
          ),
          Divider(),
        ],
      )),
    ],
  );
}



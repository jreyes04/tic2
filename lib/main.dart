import 'package:flutter/material.dart';
import 'login_page.dart';
import 'services/auth.dart';
import 'home_page.dart';
import 'root_page.dart';
import 'profile_page.dart';
import 'detailsRent_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'RAC',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: RootPage(auth: Auth()),
      routes: <String, WidgetBuilder>{
        '/HomePage': (BuildContext context) => HomePage(),
        '/LoginPage': (BuildContext context) => LoginPage(),
        '/ProfilePage': (BuildContext context) => ProfilePage(),
        '/DetailsRent': (BuildContext context) => DetailsRent(),        
      },
    );
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';

abstract class BaseAuth{
Future<String> signInWithEmailAndPassword(String email, String passw);
Future<String> createUserWithEmailAndPassword(String email, String passw);
Future<FirebaseUser> currentUser();
Future<void> signOut();

}

class Auth implements BaseAuth{
final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

//SIGN IN
Future<String> signInWithEmailAndPassword(String email, String passw) async{
  FirebaseUser user = await _firebaseAuth.signInWithEmailAndPassword(
    email: email,
    password: passw
  );
  return user?.uid;
}

//CREATE USER
Future<String> createUserWithEmailAndPassword(String email, String passw) async{

  FirebaseUser user = await _firebaseAuth.createUserWithEmailAndPassword(
    email: email,
    password: passw
  );
  return user?.uid;
}

//GET CURRENT USER
Future<FirebaseUser> currentUser() async{
  FirebaseUser currentUser = await _firebaseAuth.currentUser();
  return currentUser != null? currentUser: null;
}

//SIGN OUT
Future<void> signOut() async{
  await _firebaseAuth.signOut();
}

}
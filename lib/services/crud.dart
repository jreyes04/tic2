import 'package:cloud_firestore/cloud_firestore.dart';
import 'auth.dart';
import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_database/firebase_database.dart';
import 'package:tic_app/services/user.dart';

class CrudMethods {
  BaseAuth auth;
  User userObj;
  QuerySnapshot dataQuery;
  bool isLoggedIn() {
    if (FirebaseAuth.instance.currentUser() != null) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> addUserData(userData) async {
    Firestore.instance
        .collection('users')
        .add(userData)
        .catchError((e) => print(e));
  }

  Future<void> addCarData(carData) async {
    Firestore.instance
        .collection('cars')
        .add(carData)
        .catchError((e) => print(e));
  }

  Future<void> addRentData(rentData) async {
    Firestore.instance
        .collection('rents')
        .add(rentData)
        .catchError((e) => print(e));
  }

  String getUserData(String uid) {
    String _correo;
    FirebaseAuth.instance.currentUser().then((onValue) {
      //print(onValue.email);
      _correo = onValue.email;
      print(_correo);
      Firestore.instance
          .collection('users')
          .where("id", isEqualTo: onValue.uid)
          .snapshots()
          .listen(
              (data) => data.documents.forEach((doc) => print(doc["nombre"])));
      //Firestore.instance.collection('users').where("id", isEqualTo: onValue.uid).snapshots().listen((data) => data.documents.forEach((doc) => print(doc["apellido"])));
      //Firestore.instance.collection('users').where("id", isEqualTo: onValue.uid).snapshots().listen((data) => data.documents.forEach((doc) => print(doc["cc"])));
    });
    return _correo;
  }

  Future<QuerySnapshot> fetchUserData(String uid) async {
    Firestore.instance
        .collection('users')
        .where("id", isEqualTo: uid)
        .snapshots()
        .listen((data) => dataQuery = data

            // data.documents.forEach((doc){
            //     userObj = User(doc["nombre"], doc["apellido"], doc["id"], doc["cedula_ID"], doc["cliente"]);
            //     print(userObj.cliente);
            // })
            );
    // return userObj;
    return dataQuery;
  }
}

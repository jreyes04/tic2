import 'package:flutter/material.dart';
import 'login_page.dart';
import 'services/auth.dart';
import 'home_page.dart';

class RootPage extends StatefulWidget {
  RootPage({this.auth, this.onSignedInCallback});
  final BaseAuth auth;
  final VoidCallback onSignedInCallback;

  @override
  State<StatefulWidget> createState() => _RootPageState();
}

enum AuthStatus { notSignedIn, signedIn }

class _RootPageState extends State<RootPage> {
  AuthStatus authStatus = AuthStatus.notSignedIn;
  void initState() {
    super.initState();
    try {
      widget.auth.currentUser().then((userId) {
        setState(() {
          authStatus =
              userId == null ? AuthStatus.notSignedIn : AuthStatus.signedIn;
        });
      });
    } catch (e) {}
  }

  _signedIn() {
    setState(() {
      authStatus = AuthStatus.signedIn;
    });
  }

  _onSignedOut() {
    try {
      widget.auth.signOut();
      setState(() {
        authStatus = AuthStatus.notSignedIn;
      });
    } catch (e) {
      print("Error: $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    switch (authStatus) {
      case AuthStatus.notSignedIn:
        return LoginPage(
          auth: widget.auth,
          onSignedInCallback: _signedIn,
        );
      case AuthStatus.signedIn:
        return HomePage(
          auth: widget.auth,
          onSignedOutCallback: _onSignedOut,
        );
      default:
        return Text("something went wrong");
    }
  }
}

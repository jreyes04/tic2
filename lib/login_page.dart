import 'package:flutter/material.dart';
import 'services/auth.dart';
import 'package:sweetalert/sweetalert.dart';
import 'services/crud.dart';

class LoginPage extends StatefulWidget {
  LoginPage({this.auth, this.onSignedInCallback}); /////////////
  final BaseAuth auth;
  final VoidCallback onSignedInCallback;
  @override
  State<StatefulWidget> createState() =>
      _LoginPageState(); //create State de LoginPage
}

enum FormType { login, register } // lo usamos para cambiar de login a register

class _LoginPageState extends State<LoginPage> {
//DECLARACIONES
  final formKey =
      GlobalKey<FormState>(); // se usa para acceder al FORM desde afuera
  bool userType; //definir si es cliente o dueño
  String _email,
      _password,
      _cc,
      _name,
      _lasName,
      userId, 
      textDropDown = 'Owner';//correo, contraseña, cedula o ID, nombre, apellido

  CrudMethods _crudMethods = CrudMethods();

  FormType _formType = FormType.login; // por defecto el estado inicia en login
  ///////////////////////////////////////////////////////////////////////

  bool validateAndSave() {
    //funcion de validacion de estado para el logueo
    final form = formKey.currentState;
    if (form.validate()) {
      form.save(); //se guardan los datos una vez se verifican
      return true;
    } else {
      return false;
    }
  }

  void validateAndSubmit() async {
    //funcion asincrona de validacion
    if (validateAndSave()) {
      try {
        if (_formType == FormType.login) {
          userId =
              await widget.auth.signInWithEmailAndPassword(_email, _password);
          print('User Id: $userId');
        } else {
          userId = await widget.auth
              .createUserWithEmailAndPassword(_email, _password);
          print('Registered user: $userId');
          Map<String, dynamic> userData = <String, dynamic>{
            "nombre": this._name,
            "apellido": this._lasName,
            "cedula_ID": this._cc,
            "id": this.userId,
            "cliente": this.userType,
          };

          _crudMethods.addUserData(userData).then((result) {
            // SweetAlert.show(context,
            //     title: 'User created', style: SweetAlertStyle.success);
            print('success');
          }).catchError((e) => print(e));
        }
        widget.onSignedInCallback();
      } catch (e) {
        SweetAlert.show(context,
            title: 'Please, check the fields',
            style: SweetAlertStyle.error,
            subtitle: 'The email or password may not be correct');
        print('<------------Error------------->: $e ');
      }
    }
  }

  void moveToRegister() {
    formKey.currentState.reset();
    setState(() {
      //setState hace rebuild de la app y se mira en que estado quedo.
      _formType = FormType.register;
    });
  }

  void moveToLogin() {
    setState(() {
      _formType = FormType.login;
    });
  }

/////////////////////////////////////////////////////////////////////////

  List<DropdownMenuItem<bool>> dropDown;

  void loadData() {
    dropDown = [];
    dropDown.add(DropdownMenuItem(
      child: Text("owner"),
      value: false,
    ));
    dropDown.add(DropdownMenuItem(
      child: Text("client"),
      value: true,
    ));
  }

  @override
  Widget build(BuildContext context) {
    loadData();
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      //scafold
      appBar: AppBar(
        //appbar
        centerTitle: true,
        title: Text("Welcome to RAC"),
        // backgroundColor: const Color(0x4d6db6).withOpacity(0.5),
      ),
      body: Container(
        //container
        padding: EdgeInsets.all(40.0),
        child: Form(
          ///responsable de las validaciones
          key: formKey,
          child: Column(
            //column
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: buildInputs() + buildSubmitsButtons(), // se refactorizo
          ),
        ),
      ),
    );
  }

  List<Widget> buildInputs() {
    if (_formType == FormType.login) {
      /////////////////////////////////////////////////Refactorizar desde aquí
      return [
        TextFormField(
          decoration: InputDecoration(
            labelText: 'Email',
          ),
          validator: (value) =>
              value.isEmpty ? 'Email can\'t be empty' : null, //validator
          onSaved: (value) =>
              _email = value, // se guarda el dato en su respectiva variable
        ),
        TextFormField(
          decoration: InputDecoration(
            labelText: 'Password',
          ),
          validator: (value) =>
              value.isEmpty ? 'Password can\'t be empty' : null, //validator
          onSaved: (value) => _password = value,
          obscureText: true,
        ),
      ];
    } else {
      return [
        TextFormField(
          decoration: InputDecoration(
            labelText: 'Email',
          ),
          validator: (value) =>
              value.isEmpty ? 'Email can\'t be empty' : null, //validator
          onSaved: (value) =>
              _email = value, // se guarda el dato en su respectiva variable
        ),
        TextFormField(
          decoration: InputDecoration(
            labelText: 'Password',
          ),
          validator: (value) =>
              value.isEmpty ? 'Password can\'t be empty' : null, //validator
          onSaved: (value) => _password = value,
          obscureText: true,
        ),
        TextFormField(
          decoration: InputDecoration(
            labelText: 'CC or ID',
          ),
          validator: (value) =>
              value.isEmpty ? 'CC or ID can\'t be empty' : null, //validator
          onSaved: (value) => _cc = value,

        ),
        TextFormField(
          decoration: InputDecoration(
            labelText: 'Name',
          ),
          validator: (value) =>
              value.isEmpty ? 'Name can\'t be empty' : null, //validator
          onSaved: (value) => _name = value,

        ),
        TextFormField(
          decoration: InputDecoration(
            labelText: 'Last name',
          ),
          validator: (value) =>
              value.isEmpty ? 'Last name can\'t be empty' : null, //validator
          onSaved: (value) => _lasName = value,

        ),
        DropdownButton(
          items: dropDown,
          hint: Text(textDropDown),
          onChanged: (value) {
            userType = value;
            setState(() {
              if (userType)
                textDropDown = "Client";
              else
                textDropDown = "Owner";
            });
          },
        ),
      ];

      ///aquí se agregan mas datos (campos) de registro para insertarlos en la bd
    } ////////////////////////////////////////////////////////////////////////////////hasta aquí
  }

  List<Widget> buildSubmitsButtons() {
    if (_formType == FormType.login) {
      ///login Switch
      return [
        Container(
          child: RaisedButton(
            //button login
            child: Text(
              'Login',
              style: TextStyle(fontSize: 20.0),
            ),
            onPressed:
                validateAndSubmit, //se asume es un metodo por ende no se usa ()
          ),
        ),
        FlatButton(
          child: Text(
            'Create Account',
            style: TextStyle(fontSize: 15.0),
          ),
          onPressed: moveToRegister,
        ),
      ];
    } else {
      return [
        RaisedButton(
          //button create account
          child: Text(
            'Create Account',
            style: TextStyle(fontSize: 20.0),
          ),
          onPressed:
              validateAndSubmit, //se asume es un metodo por ende no se usa ()
        ),
        FlatButton(
          child: Text(
            'Have an account? Login!',
            style: TextStyle(fontSize: 15.0),
          ),
          onPressed: moveToLogin,
        ),
      ];
    }
  }
}
